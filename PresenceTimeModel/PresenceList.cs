﻿using System.Collections.Generic;
using System.IO;

namespace PresenceTimeModel
{
    public class PresenceList
    {
        public List<Presence> Items
        {
            get;
            private set;
        }

        public double AverageDurationInMinutes
        {
            get
            {
                double sum = 0;
                foreach (var item in Items)
                {
                    sum += item.DurationInMinutes;
                }
                return sum / Items.Count;
            }
        }

        public PresenceList()
        {
            Items = new List<Presence>();
        }

        public void Read(string  filename)
        {
            using (StreamReader reader = new StreamReader(filename))
            {
                Items.Clear();

                while (!reader.EndOfStream)
                {
                    Items.Add(Presence.Parse(data: reader.ReadLine()));
                }

                reader.Close();
            }
        }

        public void Write(string  filename)
        {
            using (StreamWriter writer=new StreamWriter(filename))
            {
                foreach (Presence item in Items)
                {
                    writer.WriteLine(item.Serialize());
                }

                writer.Close();
            }
        }
    }
}
