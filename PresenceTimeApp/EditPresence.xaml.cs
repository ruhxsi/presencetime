﻿using PresenceTimeModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PresenceTimeApp
{
    /// <summary>
    /// Interaction logic for EditPresence.xaml
    /// </summary>
    public partial class EditPresence : Window
    {
        private Presence item;
        public Presence Item
        {
            get
            { 
                return item;
            }
        }

        public EditPresence()
        {
            InitializeComponent();
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            item = new Presence(txtPupil.Text)
            {
                From = DateTime.Parse(txtFrom.Text),
                To = DateTime.Parse(txtTo.Text)
            };

            DialogResult = true;    // gibt, dass dieser Dialog mit OK beendet werden
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
