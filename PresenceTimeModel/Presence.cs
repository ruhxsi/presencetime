﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace PresenceTimeModel
{
    public class Presence
    {
        private string pupil;
        private DateTime from;
        private DateTime to;

        public string Pupil
        {
            get { return pupil; }
            set { pupil = value; }
        }

        public DateTime From
        {
            get { return from; }
            set { from = value; }
        }

        public DateTime To
        {
            get { return to; }
            set { to = value; }
        }
        public TimeSpan Duration
        { get
            {
                return To - From;
            }
        }

        public double DurationInMinutes
        {
            get
            {
                return Duration.TotalMinutes;
            }
        }

        public string Serialize()
        {
            return $"{Pupil};{From};{To}";
        }
        
        public static Presence Parse(string data)
        {
            string[] tokens = data.Split(';');

            return new Presence(tokens[0])
            {
                from = DateTime.Parse(tokens[1]),
                to = DateTime.Parse(tokens[2])
            };
        }

        public Presence(string pupil)
        {
            From = DateTime.Now;
            Pupil = pupil;
        }

        public override string ToString()
        {
            return $"{Pupil} war am " +
                $"{From.ToShortDateString()} " +
                $"{DurationInMinutes:#,##0}min anwesend";
        }

        public void AddCanvas(StackPanel stp, double height)
        {
            Canvas canvas = new Canvas
            {
                Height = height
            };

            Rectangle rectangle = new Rectangle
            {
                Height = height - 10,
                Width = DurationInMinutes,
                Margin = new Thickness(10 / 2),
                Fill = Brushes.Green
            };

            Label label = new Label()
            {
                Content = $"{pupil}: {From.ToShortTimeString()} - {To.ToShortTimeString()} = {DurationInMinutes}min",
                Margin = new Thickness(rectangle.Width + 10, 10 / 2, 0, 0)
            };

            canvas.Children.Add(rectangle);
            canvas.Children.Add(label);
            stp.Children.Add(canvas);
        }
    }
}
