﻿using PresenceTimeModel;
using System;
using System.Windows;

namespace PresenceTimeApp
{
    /// <summary>
    /// Interaction logic for MainWindow. ;xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        PresenceList presenceList = new PresenceList();
        readonly string filename = @"..\..\Presence.csv";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Presence item = new Presence(txtPupil.Text)
            {
                From = DateTime.Parse(txtFrom.Text),
                To = DateTime.Parse(txtTo.Text)
            };

            presenceList.Items.Add(item);
            item.AddCanvas(stpOutput, 40);

            tblResult.Text = $"Die durchschnittliche Anwesenheit aller {presenceList.Items.Count} Schüler beträgt {presenceList.AverageDurationInMinutes:0.00}min";
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            ClearAll();
        }

        private void ClearAll()
        {
            presenceList.Items.Clear();
            stpOutput.Children.Clear();
            tblResult.Text = "";
        }

        private void Write_Click(object sender, RoutedEventArgs e)
        {
            presenceList.Write(filename);
        }

        private void Read_Click(object sender, RoutedEventArgs e)
        {
            ClearAll();

            presenceList.Read(filename);

            foreach (var item in presenceList.Items)
            {
                item.AddCanvas(stpOutput, 40);
            }
            tblResult.Text = $"Die durchschnittliche Anwesenheit aller {presenceList.Items.Count} Schüler beträgt {presenceList.AverageDurationInMinutes:0.00}min";
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void NewEntry_Click(object sender, RoutedEventArgs e)
        {
            EditPresence dlg = new EditPresence();
            dlg.ShowDialog();

            if (dlg.DialogResult == true)
            {
                // nur wenn der OK Button vom EditPresence Dialog gedrückt wurde, dürfen
                // die Werte übernommen werden
                #region Diese Übernahme in die 3 Textboxen macht vermutlich nicht viel Sinn ;-) -> nur Demo
                txtPupil.Text = dlg.Item.Pupil;
                txtFrom.Text = dlg.Item.From.ToShortTimeString();
                txtTo.Text = dlg.Item.To.ToShortTimeString();
                #endregion

                presenceList.Items.Add(dlg.Item);
                dlg.Item.AddCanvas(stpOutput, 40);
            }
        }
    }
}
